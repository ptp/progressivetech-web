+++
title = "Voter Project"
+++

![CVH Voter tech training](/img/pages/cvh-voter.jpg)

## Voter Work Integrated with On-going Organizing

We use the term "Integrated Voter Engagement" (IVE) to describe how community organizing groups can use voter projects over multiple election cycles to strengthen their membership organizations. It is about much more than voter turnout. It is about the accountability of elected officials, mobilizing the disenfranchised and underrepresented, building an organization's base, and amplifying the voices of community leaders. Electoral projects lay the groundwork for an active and outspoken voting population that, over time, can achieve real changes for their community.
Learn from Experienced Organizers.

Progressive Technology Project's top priority is spreading the knowledge of what works and what doesn't to community organizers across the country. 

A carefully designed voter project takes a lot of preparation before anyone knocks on a door. Sometimes it's hard to envision what the result will be. 

For more information, see our [Voter Tech Kit web site](http://votertechkit.progressivetech.org/).
