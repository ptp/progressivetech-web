+++
title = "Board"
+++

![Board group shot](/img/team/board.jpg)

 * Adam Mason, [Iowa Citizens for Community Improvement](http://www.iowacci.org), Des Moines, IA
 * Jeremy Saunders, [VOCAL–NY](http://www.vocal-ny.org/), Brooklyn, NY
 * Manisha Vaze, [Neighborhood Funders Group](http://www.nfg.org/), Los Angeles, CA

*Organizational affiliations listed for identification purposes only*

