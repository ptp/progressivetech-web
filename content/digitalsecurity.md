+++
title = "Digital Security"
+++

![Training](/img/pages/elpaso-fundraising-training.png)

High profile data breaches and digital security break-ins are a regular staple of the news, usually eliciting a range of reactions in the movement from panic to complacency. Rather than let these threats intimidate us and sow fear in our organizing efforts, PTP's digital security workshops take advantage of the current climate to turn digital security into an opportunity for what we do best: organizing.

PTP's Digital Security workshops are designed to empower organizations to take advantage of their existing political education and skills and apply that knowledge to the digital security realm, allowing them to strengthen their organizing work **in the process** of learning about digital security.

PTP organizes one-off trainings for groups and coalitions, as well as multi-part series.

For more information, see our [online protection tips](https://network.progressivetech.org/online-protection) and all of our [training curriculum](https://network.progressivetech.org/trainings).
