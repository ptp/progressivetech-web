+++
title = "Technology Strategy Project"
+++

![REVERB training](/img/pages/reverb-training.jpg)

For community organizing groups, the spheres of organizing, communications, technology, voter engagement, and grassroots fundraising are increasingly becoming intertwined.

Through the **Technology Strategy Project** PTP puts our staff's combined experience within non-profit and movement building organizations together to provide deep, comprehensive and focused training and mentoring.

Our trainings have covered every topic under the sun, but with particular emphasis on:

 * **Grassroots Fundraising** - We teach and support the idea that fundraising is organizing. In any given community organization, everyone can participate, not just the development staff. To achieve that, cross training and clear definition of best data practices are key ingredients. Read our article in the September/October 2014 issue of GIFT Journal called [CRMs & Fundraising Campaings: An Integrated Approach](https://network.progressivetech.org/system/files/PTP%20CRMs%20%26%20Fundraising%20Campaigns%20GIFT%20Journal%20Sept%20Oct%202014_0.pdf)
 * **Communications (via REVERB)** - The emergence of social networks and media has transformed traditional organizing to expand to online organizing. This changes how we connect to each other, who hears our message and if they become active — REVERB helps organizations integrate BOTH their online and offline communications with a key takeaway that meaningful data does matter. Check out one of our sessions on [#BLACLIVESMATTER: Behind the Scenes of a 21st Century Movement with Alicia Garza](https://network.progressivetech.org/reverbblacklivesmatter)
 * **Voter engagement** - Using voter data provides enormous opportunities to find the most politically active members of your community, however, dealing with the vast volume of data is a struggle. Through the years we have developed key techniques and strategies for incorporating this data into your organizing campaigns. Check out our [Voter Tech Kit](https://votertechkit.progressivetech.org)
 * **Digital security** - We focus on moving organizers from fear and dread to safety and empowerment by placing politics at the center of our digital security. Understanding our political environment and organizational goals is the starting point - from there we assess our strengths and weaknesses and then investigate what changes we need to make in our digital work to continue our fight, confidently and securely. Here's one of our sessions called [Resist and Keep Safe Online: From Digital Harrassment to Harm Reduction](https://network.progressivetech.org/digital-harassment-webinar)

We have designed many variations, including multi-day, multi-organization face-to-face trainings on a single topic as well as webinar series. To get a sense, feel free to [browse our open workshop curriculum page](https://network.progressivetech.org/trainings).

However, our most effective work is via long term relationship building with organizations in which we help align an organization's politics with their technology use. This approach is highly collaborative in nature and typically includes:

* a technology assessment
* an organizational technology integration evaluation
* face to face and online trainings
* on-going consulting, coaching, and technical assistance
* Powerbase – CiviCRM database for organizers



