+++
title = "About"
+++

**Our Values:** Real systemic change will only be achieved by people of color, Indigenous people, immigrants, women, low-income people and LGBTQIA+ people who are building power to make changes in their own communities. This reality hinges on technologies that are transparent, democratic, and free from corporate ownership. Our staff and board are community organizers who offer the social justice movement alternatives to manipulative big tech, which operate in ways that run counter to the values and missions of our organizations.

**Our Story:** Over the last few decades, technology has transformed and disrupted our society in ways most of us could never have imagined. Though many social justice organizations still struggle to access much of this technology, others have begun using new tools to support their organizing, advocacy, communications, and fundraising efforts. However, the most commonly known and used technological options do not offer community organizing groups the ability to manage data securely—which presents real risks to the communities social justice organizations serve. PTP exists to offer the social justice movement an alternative to these corporate options.

Meet our wonderful [team](/team) and [board](/board)!
