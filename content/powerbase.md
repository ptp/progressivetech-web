+++
title = "Powerbase"
+++

![Jared speaks at PowerBase training](/img/pages/powerbase.jpg)

The PowerBase project brings the popular open-source CiviCRM database to organizers. We provide an integrated ecosystem to securely and reliably host, configure, maintain, and support all aspects of CiviCRM so you can easily use its full-featured toolset to help you build and engage your constituency both online and on-the-ground.

With more than 100 community and labor organizing groups participating in this project, PowerBase provides the leading online database software specifically designed for groups engaged in organizing. PowerBase offers CiviCRM as a tool that tracks what organizers want – and need – to track. PowerBase provides the training and assistance to help groups navigate CiviCRM to make it easy to share information both within organizations and with organizational partners and allies – everyone can use the same system both in the office and on the road.

Why PowerBase?

The simple reality is that until PowerBase came along there has not been a good, uniform, sophisticated database that works for both community organizers and fundraisers and that’s supported by organizing experts. The result is the situation that many groups still face:

* organizing groups waste time and effort because they have to live with multiple  databases that have been cobbled together and do not capture fully the information groups want to track
* nearly every organization uses a database unique to them, so there’s no easy way to exchange information, or receive technical support
* commercial solutions are expensive and target the ‘nonprofit’ model – community organizers have very different needs
* commercial solutions don't align with movement values, won't stand up to subpoena or use sub-par security practices

As more groups are tracking increasingly complex information, building memberships of hundreds to tens of thousands of people, engaging in voter participation efforts, and inputting data in multiple languages, the organizing community urgently needed a secure, well-designed, well-supported, uniform database system managed by a politically aligned movement partner. PowerBase is the project to consider.

As part of our PowerBase work, PTP publishes a comprehensive [set of online tip sheets on using PowerBase](https://network.progressivetech.org/).

Interested? [Find out more!](https://ourpowerbase.net/)

