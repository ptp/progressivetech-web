+++ 
title = "Team" 
+++

## The PTP Team

### Alice Aguilar, Executive Director, Austin, TX

![Alice Aguilar Head shot](/img/team/alice.png)

Alice Aguilar joined the Progressive Technology Project staff in 2008 after serving on its Board of Directors since 2002; then became PTP's Executive Director in 2011. Alice currently serves on the Board of Directors and the Coordination Committee of May First Movement Technology—PTP’s sister and ally organization.

Alice began their path as a “leftist” movement technologist first as a Database Specialist and Trainer for the Rockefeller Technology Project and then working on the introduction of ebase™ version 1.0—a database platform designed to support environmental justice organizing and advocacy efforts. Alice then transitioned to focus on providing communication technology assistance and training to BIPOC-led environmental justice groups in the U.S. southwest region and Mexico with the Environmental Support Center.

Alice worked in the social justice and non-profit sectors in Arizona as a Health Education Program Coordinator and Grant Writer assisting the Hopi Foundation, Hopi Substance Abuse Center and various Navajo and Hopi schools. She served as a Membership & Volunteer Director for the Alaska Center for the Environment, a Writer/Editor for the Eyak Preservation Council, an Executive Director for the Wrangell Mountains Center, a Development Director for the Alaska Community Action on Toxics, and as a Paralegal with Trustees for Alaska.

For over 30 years, Alice’s life’s work is grounded in supporting the rights of Indigenous Peoples and to fight for Environmental Justice and Reproductive Justice. Alice is currently organizing resistance to manipulative Big Tech and works to lead the fight against racism and sexism in technology; bringing Women, Queer and Trans People of Color into movement technology, and winning respect for the People of Color already doing technology work within social justice movements.

alice@progressivetech.org

### Jamie McClelland, Technology Systems Director, Brooklyn, NY

![Jamie head shot](/img/team/jamie.jpg)

Jamie joined the Progressive Technology Project in 2011 to support the infrastructure and development of our Powerbase Program. He is also a co-founder and serves on the Board of Directors and on various leadership committees of May First Movement Technology. Prior to working at the Progressive Technology Project, Jamie worked at Libraries for the Future as Network Administrator, National Youth ACCESS Coordinator, and Information and Technology Policy Specialist. Jamie was formerly on the Board of Directors of Paper Tiger TV where he was an active producer and activist between 1994 and 2004. Previously, Jamie worked as a Video Instructor for Sidewalks of New York, teaching basic production skills to homeless youth. He also worked as a Community Organizer for the Association of Community Organizations for Reform Now (ACORN) and was an active member of ACT UP New Orleans.

jamie@progressivetech.org

### Natalie Brenner, Director of Resource Mobilization, St. Paul, MN

![Natalie head shot](/img/team/natalie.jpg)

Natalie Brenner began her movement work with Nonviolent Peaceforce (NP)—an organization building international, unarmed peacekeeping teams in areas of violent conflict across the globe. This work solidified for Natalie the importance of taking the lead from impacted communities themselves—gleaned from the methodology used by NP—while so many other organizations were helicoptering in to provide externally developed "solutions" that ultimately harmed, rather than helped the communities they intended to serve. Natalie joined the Progressive Technology Project staff in early 2011 and while throughout her decade+ with the organization she has overseen the finances and administration, she’s also become an accidental techie; working on front-line technology support and deeply engaging with both PTP program groups and with PTP’s partners in the social justice movement. She’s always drawn on her early experiences with NP to continue taking cues not only from the organizations served by PTP, but also from the wider communities served by those organizations.

Natalie graduated Summa Cum Laude from Augsburg College in Minneapolis, MN in 2004 with a degree in International Relations and Peace and Global Studies. She serves as Co-Chair of the Coordination Committee and on the Board of Directors for PTP’s partner organization May First Movement Technology. Along with the PTP team, Natalie enjoys hosting technology skills trainings and political education sessions about liberatory technology, traveling with the PTP team to conferences and convenings to deepen relationships and grow understanding about the importance of movement-aligned technology, and always learning from movement partners, peers and communities.

natalie@progressivetech.org


