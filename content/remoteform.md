+++
title = "Remoteform Demo page"
+++

# Remoteform Demonstration page

This page is not publicly advertised. It is designed to demontrate what Remoteform can do.

## Profile

The profile example is not so thrilling because we can essentially do this with
a profile snippet, however (and this *is* big) you don't need to have PTP
process your snippet before it's ready. You really can just copy and paste the
lines of javascript code.

Here's what it looks like when you click "Settings" next to a profile, and then
expand "Advanced Settings":

![Code snippet profile](/img/pages/remoteform-profile.png)

This profile is the one we display on the Powerbase site to collect info on
people who want a demo.

<div id="remoteForm"></div>

<script src="https://ptp.ourpowerbase.net/sites/all/extensions/remoteform/remoteform.js"></script>

<script> var config = {
 url: "https://ptp.ourpowerbase.net/civicrm/remoteform",
 id: 57,
 entity: "Profile",
};
remoteForm(config);
</script>


## Finally, a simple contribution form

It only includes the price set and the fields you explicitly include in the Contribution Page profile. So, no billing address fields to slow things down!

Also, it autodetects if you are using Stripe and includes the extra lines only needed for stripe.

The code is enabled at the bottom of the Title and Settings part of the Contribution Page:

![Code snippet simple contribution](/img/pages/remoteform-contribution-simple.png)

<div id="remoteFormSimple"></div>

<script src="https://ptp.ourpowerbase.net/sites/all/extensions/remoteform/remoteform.stripe.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>

<script> 

var config = {
 url: "https://ptp.ourpowerbase.net/civicrm/remoteform",
 id: 17,
 entity: "ContributionPage",
 customSubmitDataFunc: initStripe,
 customSubmitDataParams: {
   apiKey: "pk_test_k2hELLGpBLsOJr6jZ2z9RaYh",
 },
 paymentTestMode: true,
 submitTxt: "Contribute",
 parentElementId: "remoteFormSimple",
 initTxt: "Make a contribution"
};
remoteForm(config);

</script>

## Standard contribution form

Just for comparison, this is what it looks like to use PTP's standard contribution page with no changes.

<div id="remoteFormStandard"></div>

<script> 

var config = {
 url: "https://ptp.ourpowerbase.net/civicrm/remoteform",
 id: 1,
 entity: "ContributionPage",
 customSubmitDataFunc: initStripe,
 customSubmitDataParams: {
   apiKey: "pk_test_k2hELLGpBLsOJr6jZ2z9RaYh",
 },
 paymentTestMode: true,
 submitTxt: "Contribute",
 parentElementId: "remoteFormStandard",
 initTxt: "Make a contribution"
};
remoteForm(config);

</script>


