+++
title = "Privacy Policy"
+++

We will actively fight any attempt to force Progressive Technology
Project to disclose user information or logs.

We will not read, search, or process any of your data other than to
protect you from viruses and spam, debug problems affecting our shared
infrastructure, or when directed to do so by you when troubleshooting.

Your data is encrypted. All data saved on our servers and all backups are
stored in an encrypted format.

Our policy is to immediately notify you if we learn that you are under
investigation.

To protect the privacy of our members, we regularly purge log files.

Any partner organization may request access to, correction of, or
deletion of any or all of your personal data retained by us. You may make
this request by emailing support@progressivetech.org. We will attempt to
fulfill these requests, unless legally prohibited from doing so.
