+++
title = "Movement Technology Statement"
date = "2018-02-27T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/dontblockmynet.jpg"
+++

Last Spring, the Progressive Technology Project's executive director Alice Aguilar joined 50 other movement technologists at the Highlander Center to directly confront the intersection of technology and movement politics. In a historic gathering of movement technologists (about 90 percent people of color, 50 percent women), the challenges of corporate technology domination over our progressive movements was discussed.

One result of the gathering was a [thoughtfully written petition](https://outreach.mayfirst.org/techstatement)  we are asking all movement technologists to sign.

In the words of the petition:

> Currently technology is being developed, controlled, and owned by the ruling class and used in their interests to maintain a brutal system of superexploitation and oppression. We want a shift in the underlying logic of how technology is created and used. Instead of being used as a tool to divide and conquer, we believe technology must be taken back by the people and used as a tool of liberation.

Please check it out and add your name!
