+++
title = "Facebook and Cambridge Analytica"
date = "2018-04-16T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/cambridge.jpg"
+++

To many of us, the Cambridge Analytica Facebook scandal sounds like old news: we have known that Facebook is collecting a dangerously large amount of our personal information for years. However, there is something different with this scandal and it may change the way the US movement thinks about the corporate Internet and our strategies for change.

Since the rise of centralized Internet services like Google and Facebook there have emerged two main arguments against them. 

The mainstream media has depended heavily on the consumerist and individualist argument that essentially boils down to: its creepy for corporations to keep track of what I want to buy. Predictably this approach has been a failure, because most people like having corporations suggest things to buy and the vast majority of people experience no direct adverse consequences. Furthermore, the solutions to this approach are all individualist: either leave social media or strengthen privacy controls that inevitably offer individuals marginal control over their data and keep corporate power in firm control.

For our part, radical technologists have put forward a significantly different argument: centralizing all of our data in just a handful of corporations makes it trivially easy for governments to seize the data and use it to suppress our movements. In a remarkable feat of pure luck, the Snowden leaks happened at exactly the same time that the left wing techies were making these arguments. And, the leaks confirmed (and even went beyond) the wildest paranoid rantings of the left on this topic.
And the result? It barely registered. The left continued migrating their email to Gmail and and content to Facebook in even greater numbers.

One problem with our argument in the US is that it was made during the Obama era. Despite clear evidence to the contrary, the idea that Obama would use data from Google or Facebook to suppress political movements simply felt outlandish. Furthermore, two of the most prominent movements of the period, Black Lives Matter and the Keystone Pipeline struggle, highlighted the brutal violence against people of color in the United States. The racism and segregation that plagues US society is also present on the left, and in particular within left technology circles that are the dominent voices behind the argument to build alternatives to the corporate Internet. It should come us no surprise that these voices fell on deaf ears.

Now we are in a different time. Trump offers no illusions as to what he is capable of doing. And, 10 years after the biggest economic meltdown in recent history, the left is finally starting to grapple with capitalism itself and the role of corporate America in our movements. And, most importantly, we are seeing a rise in radical technology activism from all parts of the movement (see, for example, the Movement Technologist Statement - https://outreach.mayfirst.org/techstatement).

And lastly, we have Cambridge Analytica. While the mainstream media continues to try to frame this in the individualist "creep factor" paradigm, its hard to avoid the macro implications. The Cambridge Analytica scandal demonstrates the clear link between democracy, technology and capitalism.

Our biggest concern may not be government repression, but instead corporate control of government itself via the technology we use every day. 

At this point, all the pieces for a real campaign against the corporate Internet are lined up except one: Can our most active movement organizers and technlogists build bridges over our toxic partriarchal and racist history to integrate a radical technology strategy into our movements?

Image credit: http://pusulaswiss.ch/facebook-verilerinizin-cambridge-analyticanin-elinde-olup-olmadigini-gosteren-uygulama/
