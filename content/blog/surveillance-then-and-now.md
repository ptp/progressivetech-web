+++
title = "Surveillance Then & Now: How to Protect Ourselves"
date = "2016-12-12T13:50:46+02:00"
tags = ["surveillance", "security", "cointelpro" ]
categories = ["general"]
banner = "img/blogs/cameras.jpg"
+++

![Surveillance cameras on a pole](/img/blogs/cameras.jpg "")

*By freiheitsfoo (Own work) [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), [via Wikimedia Commons](https://commons.wikimedia.org/wiki/File%3A20160712-vue-halle-saale-HBF07.jpg)*

[Resources from the webinar](https://network.progressivetech.org/21st_century_cointelpro)

*COINTELPRO destroyed movements, relationships, and activists' lives. It destroyed the trust and hopefulness needed to build and sustain a movement. It destroyed the movement for justice in this country. But this time, we have ways of fending it off and protecting ourselves.*

More than 200 listeners joined our webinar and hundreds more signed up to access the resource documents and tips we shared when PTP and May First / People Link organized a December call to discuss surveillance and what to do about it.

The webinar featured a look back at surveillance, infiltration, provocateurs and violence by government agents and talked about the new wave of surveillance built over the past 15 years and offered tools and practices that make good sense for activists’ personal protection and organizational infrastructure.

Alfredo Lopez, a co-founder of [May First/People Link](https://mayfirst.org/), recalled his own experiences with the FBI in the 1970s: infiltrating a committee planning national mobilization rally in Philadelphia during the 1976 Bicentennial, quoting statements inciting others to violence that he never made, and forcing him to move from house to house to avoid a subpoena that would have stopped him from playing a role in the rally altogether. Agents even asked his father about fabricated claims that he was involved in terrorist activity, which Lopez said gave the older man a heart attack.

As an organizer and technologist, Lopez noted, one of the big differences he sees between now and the COINTELPRO era is the quantity of surveillance today, which government agents in the 1970s could not have matched. Chris Conley, a former software engineer and now an attorney with the [Technology and Civil Liberties Project at ACLU of Northern California](https://www.aclunc.org/issue/technology-and-civil-liberties), echoed this theme.

Conley offered a good overall analysis of the situation and several useful resources, including a guide that highlights [high- and low-lights of surveillance and data protection in the tech industry](https://network.progressivetech.org/system/files/Privacy%20and%20Free%20Speech%20Primer%20-%20Volume%203.pdf) and a policy briefing on ["Making Smart Decisions About Surveillance."](https://network.progressivetech.org/system/files/making_smart_decisions_about_surveillance.pdf) The latter outlines common types of surveillance and helps community members hold government accountable for decisions about whether and how to engage in surveillance.

Driving home the point that this is happening now, Conley also shared an article from The [Intercept](https://theintercept.com/) that shows how ['covert agents infiltrate the Internet to manipulate, deceive and destroy reputations.'](https://network.progressivetech.org/system/files/How%20Covert%20Agents%20Infiltrate%20the%20Internet%20to%20Manipulate%2C%20Deceive%2C%20and%20Destroy%20Reputations.pdf).

Jamie McLelland of MF/PL and PTP offered [5 tools for activists to start using now](https://network.progressivetech.org/online-protection):

* Signal, a cell phone app for Android and iPhones that provides end-to-end encryption of text messages if you and the person you are texting both use it (otherwise, it works like a regular texting app).
* Full Disk Encryption: scrambling the contents of your computer in a way that can only be decrypted with a password.
* Password Manager: provides a secure place to store a different, long password for every site that you visit.
* ownCloud: file, calendar and contacts sharing service that allows you to share a folder on your computer or your phone that is synchronized with anyone you want–more securely than commercial products such as Dropbox.
* PowerBase: our database is hosted by us on our own servers; PTP and May First/People Link have a history of both supporting and actively participating in our movements.

Want more info? You can review [all the materials related to the webinar here](https://network.progressivetech.org/21st_century_cointelpro). Or read one of our recent blog posts on Keeping Safe: Movement Organizations Face Up to Attacks and Surveillance and We’re Going to Need To Own the Internet on surveillance, people of color techies, and racist algorithms.

