+++
title = "Progressive Technology Project is leaving Twitter "
date = "2022-12-12T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/twitter-dumpster-fire.png"
+++

PTP is happily joining the migration from Twitter to non-corporate alternatives! Many of you have already launched your new home in the fediverse and we invite you to [follow us on Mastodon here](https://social.coop/@ptp).

For those of you still using Twitter, we invite you to learn about the alternatives by signing up for an account on one of the many fediverse instances (see below for more information on what the fediverse is and how to join it).

## Wait, shouldn't we fight for Twitter?

Twitter has and will continue to be an arena of struggle for the left. We should always fight against racism, misogynony, homophobia and all forms of oppression wherever they arise. However, as with all tactics we have to ask: Toward what goal? Can Twitter be reformed?

Elon Musk's purchase of Twitter was not irrational - it is a calculated recognition that digital surveillance has become a critical component of capitalism. At this point, it's unclear what will happen to Twitter. Will it degenerate into a cess pool of hate and right wing conspiracies? Or will it stabilize, thus becoming a more profitable and effective source of digital surveillance for Musk? Check out this [article exploring Musk's underlying nefarious goal for the platform](https://www.project-syndicate.org/commentary/musk-bought-twitter-to-get-cloud-capital-by-yanis-varoufakis-2022-11).

Should we fight for a better Twitter or should we strengthen and develop our own tools?

Progressive Technology Project has spent two decades working towards movement-aligned alternatives to corporate technology that are secure and that protect rather than capitalize on the data of our movements.

Since social media's inception there have been major red flags, warning signs and public bombshells that have pointed to the dangers of relying on corporate social media that is essentially designed to surveil us and grow wealthy on our human capital, but this latest situation with Elon Musk's purchase of Twitter presents an "in our face" danger that has many of our organizations asking: what are our options? There are non-corporate, federated alternatives to Twitter (and Facebook, Instagram, TikTok, etc.) out there and it's a really good time to explore those options.

## What is the fediverse and how is it different?

The Fediverse means instead of having just one site hosting the software, you can have as many sites as you want - and they can all inter-operate while still maintaining their own autonomy. In this way, the "fediverse" is similar in design to email: everyone can have a fediverse account with any provider you want. And you can still follow and be followed by anyone anywhere.

The main problem facing the fediverse has been the lack of movement activists using it. If everyone you know and want to know is posting on Twitter, it can be quite lonely in the fediverse. But now that has changed. Elon Musk is like a horrible gift that has finally tipped the balance and caused a critical mass of activists to move to the federated future.

## Sign me up

PTP chose [Social.coop](https://social.coop/) as our instance because of their commitment to democratic technology and collective ownership. Unlike many instances (which are free and/or depend on voluntary donations from you), social.coop does require a sliding-scale monthly payment, however, in exchange you become a formal cooperative member.

On the other hand, you can choose from many options (see this [un-official list here for ideas](https://joinfediverse.wiki/Special_interest_instances)). When choosing an instance, be sure to browse the server rules. Most instances have an about page (e.g. [here's social.coop's about page](https://social.coop/about)) where you can browse the server rules and get a sense as to whether they share your values.

Once you have an account, you will likely find it quite similar to Twitter - you can post your thoughts and find people to follow. You can "boost" and "like" and, most importantly notify with @ - so if any of you join, [send us a shout out on Mastodon here](https://social.coop/@ptp).
