+++
title = "Voices of Resistance: Using technology as a movement-building tool"
date = "2018-04-23T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/alice-blog.png"
+++

Progressive Technology Project Executive Director Alice Aguilar has been interviewed by Rebekah Barber on [Facing South](https://www.facingsouth.org/)! Check out the [full interview](https://www.facingsouth.org/2018/04/voices-resistance-using-technology-movement-building-tool).

From the Intro:

> This week the nation's attention was focused on the nefarious ways technology is deployed, as Facebook founder and CEO Mark Zuckerberg testified before Congress about how his company has violated the privacy of its customers and allowed the vast amount of data it collects from them to be used for ethically and legally questionable political propaganda efforts.

> But technology also has tremendous potential to serve as a tool for political liberation, says Alice Aguilar. As a woman of color in the white male-dominated tech industry, Aguilar knows firsthand what it is like to face discrimination. In her position as executive director of the Texas-based Progressive Technology Project, she works alongside communities of color to find ways to use technology as a tool to build power.


Be sure to read the [full interview](https://www.facingsouth.org/2018/04/voices-resistance-using-technology-movement-building-tool).
