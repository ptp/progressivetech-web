+++
title = "Is it important to control our communication tools today? What about tomorrow?"
date = "2023-08-09T11:27:08-04:00"
tags = ["surveillance"]
categories = ["general"]
banner = "img/blogs/antifa.jpg"
+++

![Antifa Graffitti](/img/blogs/antifa.jpg "")


Many of us have seen the recent headlines about Zoom using audio and video
content from calls to train its Artificial Intelligence (AI) models. Some
aspects of this story are familiar, but there are a two angles that are worth
considering more carefully. 
 
**The first is access.** Can zoom access our conversations? Despite Zoom's best
marketing effort in 2020 [claiming they are rolling out end-to-end
encryption](https://blog.zoom.us/zoom-rolling-out-end-to-end-encryption-offering/),
the truth is that virtually none of our zoom meetings are end-to-end encrypted.
If you record the meeting, have break out rooms, use live captioning and many
other features, [end-to-end encryption is not even
possible](https://ptp.ourpowerbase.net/civicrm/mailing/url?u=20211&qid=857522).
And that's not because Zoom hates privacy, it's because end-to-end encryption
prevents them from monetizing our meetings. The recent AI-related changes in
the Zoom terms of service are further confirmation that *our content on
corporate communications platforms is and will always be accessible to those
corporations and anyone else they provide access to in their quest to monetize
our data.* This is bad, but it's not that bad until you consider the second
angle.
 
**The second angle is the political environment.** In the last eight years we have
experienced a president with an unprecedented disregard for constitutional law
that is well positioned to be re-elected, a sustained and effective national
book burning and curriculum-cutting campaign targeting traditionally
marginalized communities, a resurgence of electoral manipulation blatantly
designed to disenfranchise African Americans, a reversal of a landmark court
case on reproductive justice and countless other developments pointing to a
potentially unprecedented future of repressive government control in the United
States.
 
These two developments are on a collision course that is particularly worrisome
for any movements that depend on Internet communications. 
 
Fortunately, there is a silver lining. There is a growing movement of
technologists working daily to build alternatives to corporate communications
technology--a movement that everyone organizing with Powerbase, joining
**Progressive Technology Project** calls and trainings via Jitsi Meet, or using any
of the hundreds of other movement-run technology services is part of. 
 
**Thanks to all of you we are exploring entirely different models for how to
collectively build technology based on the principles of mutual aid,
collaboration, privacy, respect and sustainability.** While our alternatives are
not always as shiny or sophisticated as the corporate tools, together, with an
eye to the future we are defining what really makes one piece of software
better than another.


*Photo credit: By Jcarax68 (Own work) [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0), [via Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Antifa_2.jpg)*
