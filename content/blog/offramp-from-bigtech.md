+++
title = "How nonprofits can off-ramp from Big Tech"
date = "2025-02-10T12:47:08+02:00"
tags = [""]
categories = ["general", "bigtech"]
banner = "img/blogs/offramp.jpg"
+++


![Arial view of the a complicated set of freeway on and offramps](/img/blogs/offramp.jpg)

*Image credit: https://commons.wikimedia.org/wiki/File:Jane_M._Byrne_Interchange_4-1-22.jpg, license: https://creativecommons.org/licenses/by-sa/4.0/deed.en*

*This blog was originally posted [on NTEN](https://www.nten.org/blog/nonprofits-off-ramp-big-tech).*

Many of us invested countless hours, months, and years learning how to use
Twitter and Facebook to build followers, promote our organizations, and engage
and mobilize new folks with our projects and campaigns. The Progressive
Technology Project even held organizing and strategic communication trainings
to help nonprofit organizations avoid getting left behind during the Web 2.0
mania.

Fast forward a decade and Twitter, now renamed X and owned by Elon Musk, has
gotten so bad that the [Atlantic has officially declared it a "White Supremacist
Site"](https://www.theatlantic.com/technology/archive/2024/11/x-white-supremacist-site/680538/).

Never one to be left behind, Facebook’s parent company Meta has [contributed a
million dollars to Donald Trump’s
inauguration](https://www.forbes.com/sites/siladityaray/2024/12/12/meta-donates-1-million-to-trumps-inaugural-fund-weeks-after-ceo-zuckerberg-met-president-elect/)
while announcing the [elimination of its fact checking team](https://theintercept.com/2025/01/07/facebook-fact-check-mark-zuckerberg-trump/) in an obsequious
capitulation to Donald Trump.

Being seen on X is not only unhelpful, but has become a liability. And, given
the direction of Meta, Facebook is not far behind.

Are X and Facebook exceptions, or is this the future of Big Tech?

Silicon Valley, once known for its support of liberal causes, has pivoted to
the right; a move accelerated and exacerbated by Trump's victory. This change
is largely fueled by economic realities. Big Tech corporations are some of the
largest companies on the planet, now representing [three of the top ten largest
corporations in the world](https://www.forbes.com/lists/global2000/) - Amazon,
Microsoft, and Google.

> What does this trend towards the right mean for our organizations when we not
> only use these technologies, but many of us are downright dependent on them?

How does a technology company worth billions of dollars stay on top when
strategies like inventing yet another new app or adding even more features to
Gmail are played out? They pursue [military and police
contracts](https://theintercept.com/2024/11/17/tech-industry-trump-military-contracts/).
The [Pentagon has signed multi-billion dollar
contracts](https://www.cnn.com/2022/12/08/tech/pentagon-cloud-contract-big-tech/index.html)
with Big Tech, [Amazon makes no effort to hide their military
ambitions](https://aws.amazon.com/federal/defense/), and [Project
Nimbus](https://www.theverge.com/2024/12/3/24311951/google-project-nimbus-internal-documents)
includes a deal between Google and the Israeli Ministry of Defense worth $525
million dollars.

In the case of Amazon, their facial recognition contracts with the police were
so offensive that a mass organizing effort [forced the corporate behemoth to
pause the program back in 2020](https://www.technologyreview.com/2020/06/12/1003482/amazon-stopped-selling-police-face-recognition-fight/). But now, with every Big Tech giant pursuing
artificial intelligence, we can expect [more and more contracts with police and
military forces](https://www.technologyreview.com/2024/11/19/1106979/how-the-largest-gathering-of-us-police-chiefs-is-talking-about-ai/) around the world.

And now we hear Mark Zuckerberg’s announcement of [the end of independent
fact-checking for
Facebook](https://www.nbcnews.com/tech/social-media/meta-ends-fact-checking-program-community-notes-x-rcna186468),
including ending most content moderation policies on what he called
"hot-button" issues such as immigration and gender. Zuckerberg has [changed his
tune to parrot right-wing MAGA talking
points](https://www.npr.org/2025/01/07/nx-s1-5251151/meta-fact-checking-mark-zuckerberg-trump)
with statements like "the company's content moderation approach resulted too
often in censorship".

Many in the nonprofit sector are raising the alarm. What does this trend towards the right mean for our organizations when we not only use these technologies, but many of us are downright dependent on them? Will other social media platforms like BlueSky meet our needs in the same way, or will they fall to the right as well? What if artificial intelligence-powered algorithms flag our content as too political, banning our accounts and cutting off our organizing before we even have an opportunity to download our data? Will Trump further erode privacy laws and, in what seems like a growing partnership with Big Tech, increase surveillance of our internet use, our email, and our data?

## Why aren’t more of us leaving corporate technology behind?

For the 25 years that Progressive Technology Project has been doing technology justice work, one of our major focal points has been on increasing understanding about why Big Tech is so detrimental to our movements, our organizations and our communities. Over the past 10 years as the U.S. and global political climate has pivoted to the right and it’s become more widely known that Big Tech is in bed with right-wing extremists, we’re seeing some organizations reach a deeper understanding about the importance of embedding technology into their wider strategies. But, even as organizational leadership acknowledges that a move away from Big Tech is needed, the number of groups actually committing to transitioning doesn’t match the level of alarm. Why is this? Over the past 6 months, the Progressive Technology Project has been conducting a research project to ask the movement this very question so that we—and our network of technology justice partners—can be better prepared to help.

Our research surveyed 460 individuals and organizations, and we focused our questions to ask about the barriers to leaving Gmail and Google so that we could get specific data that could then be applied beyond Google and Gmail transitions. Our [initial findings](https://mayfirst.coop/en/audio/cutting-the-cord/slide-show.pdf) indicate that the biggest barrier to leaving Google is not a lack of features or concerns over the stability of alternatives, instead it's the fact that everyone else is on Google. Through follow-up interviews, we found that people had similar reasons for hesitating to leave Facebook, Zoom, and Apple. The top three additional barriers to leaving Big Tech services like Google included 1) a concern about whether all of a person’s data would transfer seamlessly to a new system, 2) people felt they had too much data to make a transition, and 3) people were concerned that an alternative, non-corporate technology would not be able to integrate with other systems the way that their current technology tool could (for example, people use their Google information to log into masses of other tools and websites).

> The biggest barrier to leaving Google is not a lack of features or concerns
> over the stability of alternatives, instead it's the fact that everyone else
> is on Google.

Encouragingly, people were not generally concerned about the security of non-corporate technologies, and we found this was reinforced in our follow-up interviews, where respondents expressed that alternative technologies were probably more secure than Big Tech tools. People also thought that alternative technologies had no issues with government and other compliance requirements, and they felt that non-corporate options were stable and reliable.

This research is proving incredibly valuable as PTP and our partners work to strengthen the network of values-aligned technologies and providers, add and improve functionality for the technologies themselves, and create more capacity to help organizations transition away from the technologies owned by the very same Big Tech corporations that they fight. 

We’ve got a whole network of providers poised to help organizations start transitioning away from Big Tech, but what if you’re not officially ready for that step? What can you and your organization do to start thinking about technology more strategically and take steps towards a transition?

## How to find the Big Tech off-ramp

Building political education about technology strategy into your regular organizational planning and discussions is a good place to start. Thinking about technology as a tool in a silo is not helpful. Thinking about technology politically as an integral part of your campaigns will help increase understanding among your staff and teams about the importance of this issue. Many people don’t know that Google has contracts with the Israeli military or that Amazon assists the police with improving facial recognition software meant to surveil us or that Zoom is using AI to comb through our meeting data. Without political education and a concerted effort among all of the stakeholders in your organization to see a transition through its inevitable learning curves and pain points, any technology transition is unlikely to be successful.

While Big Tech options can seem cheaper and sometimes prettier than some of the alternatives, their costs—both monetary and otherwise—are much higher.

Secondly, it is important to start slowly. Go for the low-hanging fruit. Let's face it—technology transitions are painful, and the learning curve is usually steep. People are comfortable with what they are already using, and as we saw from our research, are comfortable that everyone else is using it as well. This may mean that your organization keeps all of your current technology and doesn’t mandate some huge, immediate change, but starts to use an open-source collaborative note-taking pad for some virtual meetings like those offered by [Riseup](https://riseup.net/). Or, instead of using Zoom for every meeting, try using the alternative [Jitsi.meet](https://jitsi.org/) or [Big Blue Button](https://bigbluebutton.org/) for smaller meetings to see how you like it. [Nextcloud](https://nextcloud.com/) is an open-source alternative to Google Docs that you could try out for specific, non-urgent tasks, and organizations like [Cloud68](https://cloud68.co/index.html) can help host and support Nextcloud for you if needed.

Transitioning to using new technology will require a cultural change in your organization—and this will take time. A strategy that can be useful and sustainable during the early phases of considering a technology transition is to develop organizational policies and protocols on how and when to use different communication channels. For example, it may be critical for your organizers to use [Signal](https://signal.org/) rather than texting through corporate phone providers for sensitive communications during mobilizations or protests. Building internal agreements about what information can or cannot be shared, and creating policies describing when it’s okay to use corporate platforms for general outreach and announcements are also examples of what to consider.

Also read: [Data empowerment for nonprofits](https://www.nten.org/publications/data-empowerment-report).

As you and your staff slowly grow comfortable with some of the alternative options and learn that the politics of many of these options are aligned with yours, you may consider taking the next step. Perhaps you could decide to transition just one of your main technology systems to a liberatory alternative. If you use a [Bonterra](https://us1.campaign-archive.com/?u=23c640fc15bca8deabcbdc872&id=bd778d92fe) CRM database for example, explore what a transition to [CiviCRM](https://civicrm.org/) or [Powerbase](https://powerbase.net/) would look like. Plan ahead and get input and feedback from every part of your organization, making sure you commit to the time and resources it takes to truly integrate a technology into your work (which includes loads of post-transition time and dedication and a commitment to seeking ongoing support). Maybe you choose to transition all of your virtual meetings from Zoom to Jitsi or Big Blue Button. Or you move your organizational email away from Gmail to [May First Movement Technology’s email](https://mayfirst.coop/).

> A strategy that can be useful and sustainable during the early phases of
> considering a technology transition is to develop organizational policies and
> protocols on how and when to use different communication channels.

Another step you could take is to build technology strategy more thoughtfully into your budgets. While Big Tech options can seem cheaper and sometimes prettier than some of the alternatives, their costs—both monetary and otherwise—are much higher. That being said, the network of liberatory alternative technology organizations and providers is grossly underfunded and under-resourced. We need to start divesting from Big Tech and investing in these liberatory alternatives to scale their viability for the movement and to do that, we need funders to pay attention to technology strategy. If organizations build budgets and fundraising efforts that include technology strategy, it will go a long way to lift up movement technology.

Finally, as you begin thinking about a technology transition away from Big Tech and towards open-source and movement-aligned alternatives, it helps to get lots of support. Organizations like [Progressive Technology Project](https://progressivetech.org), [May First Movement Technology](https://mayfirst.coop/), the [Electronic Frontier Foundation](https://eff.org/), [Media Justice](https://mediajustice.org/), the [Association for Progressive Communications](https://apc.org/), and many more can help point you in the right direction whether it be about strategy or finding alternative tools. The Electronic Frontier Foundation has one of the best resources for learning about the importance of digital security, and the why and how of technology transitions - their [Surveillance Self Defense article](https://ssd.eff.org/).

> The network of liberatory alternative technology organizations and providers
> is grossly underfunded and under-resourced. We need to start divesting from
> Big Tech and investing in these liberatory alternatives to scale their
> viability for the movement and to do that, we need funders to pay attention
> to technology strategy.

Any conversation about technology transitions inevitably becomes about the
tools. But the meaning behind the technology we use goes much deeper than that.
It is where the data about our constituents’ immigration status is stored. It’s
where we track cases about reproductive health needs. It’s where we post our
political viewpoints for all the world to see. It’s where we store our credit
card information. It’s where we text our best friends about our deepest fears.
Progressive Technology Project and our partners work tirelessly to make this
data about us—about every part of our lives—safe, more secure, and firmly in
the hands of the people, rather than with Big Tech billionaires using
extractive surveillance of our lives to increase their power and profit and to
censor our speech and track our actions.


