+++
title = "Can we build alternatives to corporate technology?"
date = "2023-05-01T08:47:08-04:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/what-tech-do-you-depend-on.png"
+++

May First Movement Technology has released a report: ["Politics and practices
of an autonomous technology: voices from the May First
membership."](https://mayfirst.coop/en/post/2023/autonomous-technology/) Based
on extensive interviews, the report explores why and how social justice
movement organizers and activists are building a sustainable and powerful
alternative to corporate technology.

According to Alice Aguilar, executive director of Progressive Technology
Project and one of the authors of the report:

> It's not possible to have a truly collective approach to technology without
> cultivating a radically inclusive space that is representative of our
> movements. **For us, building autonomous technology and movement organizing go
> hand-in-hand - we can't win without understanding how inseparable they are.**

The report features interviews with organizers in the US, Mexico, Canada, the
UK and Colombia engaged in working on issues ranging from media to abortion
rights to community organizing. The content centers on the experiences of
building collective technology, but also includes the challenges of organizing
across borders and languages, creating inclusive and democratic processes and
cultures, and integrating the diverse politics of the left. In other words,
it's about the future of our movement.

We invite you all to [check it out](https://mayfirst.coop/en/post/2023/autonomous-technology/) and help spread the word!

The report is available in both [english](https://mayfirst.coop/en/post/2023/autonomous-technology/) and [spanish](https://mayfirst.coop/es/post/2023/autonomous-technology/).
