+++
title = "On a 'Should I Freak Out?' Scale, Friday's DDoS Notches an 8"
date = "2016-10-25T21:49:20+02:00"
tags = ["ddos"]
categories = ["general"]
banner = "img/blogs/level-3-ddos-map.png"
+++

The revolution may or may not be televised, but last week someone clearly tried to start it by TiVo.

![DDos 10-21-16 outage map of us](/img/blogs/level-3-ddos-map.png)

*Tech company Level 3 offered this outage map*

You probably read that it was mostly hacked DVRs connected to the Internet that were used to launch the DDoS (Distributed Denial of Service) attack on the Internet infrastructure Friday, Oct. 21.  Wired [provided a good, WTF analysis](https://www.wired.com/2016/10/internet-outage-ddos-dns-dyn/).

Access to Powerbase was not affected.

So, freak out or not?

You might be inclined to let the news pass without much attention - it’s less than 16 days to a presidential election after all. And in fact the problem got fixed relatively quickly for most people.

But the incident is more worrying considered in context. This is not the first, just the biggest and most effective in a new wave of massive attacks on the web. An article for a more tech-oriented audience last month suggested people ["should be prepared for attacks that have the ability to disrupt ever bigger swaths of the Internet."](http://arstechnica.com/security/2016/09/botnet-of-145k-cameras-reportedly-deliver-internets-biggest-ddos-ever/)

How did we get here? Is the Internet naturally vulnerable to this kind of attack?

We've arrived at this precarious spot largely due to rapid, profit-fueled growth that has placed a priority on the bottom line over developing a secure and robust Internet. 

Most of the devices used in this attack were shipped and put into production with the same default password (for some devices it was "admin" or even "1234"). Even the greenest, technologically naive activist know better than this! Other attacks take advantage of corporate networks that refuse to implement a simple anti-spoofing protocol that was introduced over 15 years ago. 

This is not a problem with the Internet, but it is a problem with how it has developed. And, despite the latest attack briefly taking down some of the biggest corporations on the planet, it's not a catastrophe for the corporate Internet. They have the resources to react: the cash to hire additional tech help, the leverage to force upstream providers to drop everything and fix the problem and the communications capacity to get the word out about what's happening in real time.

Most observers think this is part of a trend toward more people trying harder to take down the Internet, whether for money, politics, or bragging rights. 

As the attacks grow bigger and more powerful, only the largest and most capitalized corporations will be able to defend themselves. Smaller organizations and independent voices may be swamped or could even disappear forever if they were targeted for the kind of takedown we saw Friday. It was their turn last week but the power of the DDoS attack last week threatens all of us. 

Internet security watcher [Brian Krebs wrote about this just a few weeks ago](https://krebsonsecurity.com/2016/09/the-democratization-of-censorship/) (and has continuing coverage of the Internet of Things DDoS attack on his site). 

PTP is an advocate of the decentralization of the Internet, technology and big data, which would circumvent the risk by reducing reliance on the Amazons and Googles (and yes, the Dyns, too) of the world.

Meantime, every major attack on the Web like this ought to remind us to take basic security precautions: update software to the most current available version, log out when you’re done, change your passwords from time to time. 

[Mashable has a good analysis of what happened and what we can do to prevent something like it in the future here.](http://mashable.com/2016/10/21/protect-home-devices-iot-hacks/?utm_cid=hp-hh-sec#29AltQhE9SqR) 

Were you affected by Friday’s DDoS? Let us know by [email](mailto:communications@progressivetech.org).

--Gordon Mayer_


