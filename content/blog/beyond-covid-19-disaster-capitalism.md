+++
title = "Beyond COVID 19 and Disaster Capitalism: Why We Need Sustainable, Secure Left Tech Infrastructure for Social Justice Movements"
date = "2020-05-06T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/reset-internet.jpg"
+++

*by Jeremy Saunders, Co-Executive Director of [VOCAL-NY](http://vocal-ny.org)
and Board Chair of Progressive Technology Project, and Alice Aguilar, Executive
Director of Progressive Technology Project*

![Picture of ethernet jack and reset pin hole](/img/blogs/reset-internet.jpg)

*Image credit: “Photogamer — Jan 22, 2008” by RedRaspus is licensed under CC
BY-NC-ND 2.0*

Covid-19 has exposed the failures of our health, economic, and emergency
response systems. Our priorities now are to take care of ourselves and one
another, creating communities of care. Front-line movement groups are
organizing to respond to the immediate needs of their communities, while
struggling to move much of their on-the-ground organizing work online. And once
again, as we’ve learned from the history of disaster capitalism, corporations
are taking advantage of our need for online engagement and profiting while we
“shelter-in-place.” Corporations are marketing their online tools — such as
Zoom — to our movements under the guise of ‘helping’, when instead they are
wreaking havoc on our data security and privacy as we’ve seen from
“zoom-bombing” and as they sell our personal information to third party
vendors. The move to online is also exposing a serious lack of tech
infrastructure, and more importantly, lack of access to the internet, which our
communities need to stay connected. We may soon witness communities that will
“disappear” (and we may not know it) as their voices are repressed because of
this lack of access.

The power of the technology sector is impossible to ignore. We are greeted by
daily headlines on the profits reaped through data collection by tech giants,
and the power of the information, disinformation and misinformation when this
data is weaponized. While tech giants spin this digital age as one of global
interconnectedness and simplicity, they simultaneously amass wealth, political
power and surveillance data that will allow them to accumulate more wealth and
political power. They fight attempts to be reined in or regulated. Meanwhile,
our elected officials continue to fumble or cravenly capitulate, revealing just
how unprepared they are to manage these monopolies on behalf of the public. The
technology industry is so rapidly advancing that we collectively cannot keep
pace.

It was not always like this. While the internet began in the 1970s mostly for
military and educational use, the left quickly joined. “Left technologists” —
those who use and develop technology as a key tool to advance social justice
movements — began developing systems of digital communications for
transnational organizing. Early examples of their work include global efforts
to end South African Apartheid and engagement with Latin American movements
like the Sandinistas and the Zapatistas. Left technologists developed new tools
based on an understanding of the technology needs of organizers. Their
collective actions were used to rein in concentrated power from a handful of
corporations.

In the 1990s, during the first dot com Internet boom which was kicked off by
the invention of World Wide Web, another development was taking place that
would have a profound impact on the left’s relationship with technology and the
Internet. New transnational movements sparked conversations about corporate
wealth extraction through trade agreements. In the lead-up to the 1999 World
Trade Organization meeting in Seattle, activists — mostly Indigenous,
environmentalist, labor and anti-Capitalist — began planning waves of mass
protests that they hoped would shut down the city and the trade talks
altogether. Critical to this coordination was secret organizing across
geography. Given organizers’ fears that corporate media would ignore or
misrepresent their message, it became essential for social justice movements to
develop their own web-based forms of mass communication. The decreasing costs
of video cameras and laptops provided key digital tools. Just as the early
global solidarity groups created technology for transnational communication and
support, this new generation created the world’s first federated network of web
sites that anyone, anywhere could use to upload news, pictures and video.
Called the Indymedia Center, it spread across the globe, with
geographically-based collectives of individuals launching and managing news
sites from Latin America to Asia, Africa, Australia, Europe and North America.

While the birth of Indymedia is a milestone for the Internet, its true
significance lies in its relationship with the movements: the left built the
technology it needed, where it did not exist before, with technologists and
activists working together toward a common goal.

As the Indymedia network spread, corporations and governments took notice.
Profit could be reaped here. Voices could be silenced and people could be
watched.

Left technologists operated on shoestring budgets, working in their spare time
and often in widely dispersed small circles; of necessity and principle, they
shared their software using open source licensing. Corporations took these left
technologists’ innovations and commodified them; developing and refining their
own versions for profit. It is unclear if the corporations even knew in those
early days the driver of true power and wealth they were developing; power and
wealth not only from the technology but also from the constant stream of
information and data that could be harvested and sold for profit making,
opinion shifting or worldview manipulating.

As corporate tools became sleeker and more cleverly marketed — often initially
for free — progressive institutions and social justice movements began to
utilize them, sometimes even renouncing the tools that left technologists had
built for them. And why not? There were problems with these tools. Due to
under-investment in their budgets (profit was generally not a motive and
“philanthropy” preferred a market solution over open source work), the tools
they developed lacked intuitive design and smooth processing. They were created
by the tech savvy on the Left, but there was another important limitation to
the effectiveness of this group. At the time, it consisted of mostly white men
based in the United States. People who had the access and education to utilize
these tools with ease were from the same class and ethnic background.

Corporate built tools were inherently simpler, user tested and built around the
consumer experience. Additionally, more people globally were using them,
thereby allowing social justice movements and organizers to connect with a
larger circle of people. Movement leaders began to consider using these same
tools their opponents had, lamenting, “Why can’t the Left also have nice
things?”

By the beginning of the Great Recession, some elected officials started testing
these corporate technologies to shape worldviews, and in 2007 the Obama
campaign put together the most technologically advanced voter engagement
program ever seen. They mined data and brought about a transformation of the
entire Democratic Party. And progressives and the non-profit industrial
complex, even those on the Left, would follow; building similar community
engagement infrastructure by using corporate technology for their power
building efforts and to win major victories for progressive social justice
movements.

The movements’ love affair with corporate technology during this period was
pervasive and ingrained. It would be under the Obama administration that our
fears about our government’s system of mass surveillance was confirmed, after
the now infamous security information shared by Edward Snowden became public in
2013. Yet the response from the left was relatively muted. The left continued
moving to Gmail, using Facebook and depending on Twitter to spread our
messages.

Meanwhile, corporate technologists continued to extract massive wealth through
harvesting people’s information — made possible because we never read the fine
print. And wasn’t technology just making everything easier?

Like the rich and powerful before them, these tech giants hired legions of
lobbyists, avoided taxes, and ignored questions and criticisms of privacy
intrusion. Their wealth became so great that their industry alone began to
gentrify communities and own whole markets and supply chains. They have also
become a major supplier of data for mass surveillance.

Only after the Trump election in 2016 have the movements slowed down and begun
questioning the use of corporate technology, as we now clearly see the dangers
of entrusting profit-seeking corporations to manage such an important resource.

Today, most progressive organizations and individuals globally utilize
corporate technology. While we try to demand that corporations pay their fair
share of taxes, we enrich an entire sector that refuses to do so by using their
services. And — knowingly or not — we add to their constantly growing control
of our data all the way up to a steadily centralized system of corporate
controlled servers that have ultimate access and power over all information.

To fight the extent of corporate control of our information is not a question
of personal responsibility or what impact, if any, our individual actions will
have if we boycott corporate technology. What we need is collective action to
divest from corporations and invest in our communities. This requires us to
refute the apathy that ensues when we say, “It’s already done. We can’t go
back. They already have our lives in their systems. And life is simpler now.”
Just as technologists before us have done, we must (and we can) build the
alternatives. As progressive and left organizations — built to fight oppression
and win justice — we cannot use the same tools that enrich and centralize power
to a tiny set of corporate tech giants committed only to their bottom line —
profit — who are undemocratic and unapologetic in their refusal to be
transparent or regulated.

Or, maybe more simply, we can utilize and spread the knowledge and technology
infrastructure that we already have. Thankfully, left technologists still
exist. They have created free and open source digital tools that link to secure
servers, they have a commitment to ensuring that our data does not get used for
surveillance or profit, and they work collaboratively and in solidarity with
social justice movements worldwide.

Nonprofits, cooperatives and collectives have worked tirelessly for years to
build an alternative Internet infrastructure that mirrors the movements’
values. Organizations like the Progressive Technology Project, May First
Movement Technology, Riseup and many more around the world offer tools, servers
and services for the movements, and need the participation and collaboration of
the left not only to continue building these tools, but to jointly design a
strategy for our emancipation from the corporate Internet.

While these tools are not always as sleek and intuitive as the corporate tools,
the more resources we collectively bring to bear through individual and
philanthropic investment, the more we can create technology on our own terms
and for our own uses — and advance justice, equity and love without the fear of
surveillance and the lament of enriching corporate giants.

And to the question, “why can’t the Left also have nice things?” I offer a
story. In the 1980s, in an attempt toward self-sufficiency, the Zapatistas
movement developed coffee cooperatives and a distribution chain to move the
coffee to markets in the United States. It became a huge system developed by
people impacted by egregious social inequality and benefited social justice
movements with a focus on justice and equity. With increasing notoriety for
this coffee, a writer of The Nation bought a bag and brewed a cup.
Unfortunately, they did not like it. After just that one cup of coffee, they
penned an opinion piece lamenting this same question, “why can’t we have nice
things?”

The Left does have nice things. Beautiful and amazing things. We’ve built
powerful technology that links people together across the globe to fight
corporate greed on issues like housing, voter rights, and worker justice, as
well as climate, drug and immigration policy. But corporations have commodified
these things and now have a level of control over our technology that we did
not think possible. We must divest from these corporate tools and invest in our
own. And before criticizing the — often minor — functionality, we must remember
the real cost.
