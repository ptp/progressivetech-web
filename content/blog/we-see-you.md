+++
title = "We See You. We Stand With You. Black Lives Matter."
date = "2020-06-05T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/black-lives-matter-sign.png"
+++

We at the Progressive Technology Project stand in solidarity with Black-led
protests in Minneapolis and other cities around the country in response to the
murder of George Floyd by police. We stand with movements rising up to resist
police and state violence. We stand in solidarity in the fight for the lives
and liberation of Black people...Indigenous peoples...and People of Color
Communities.

We believe that change comes from the grassroots, through movements led by
people of color from the global majority who have been systematically excluded
from the power and wealth that they have produced. The Progressive Technology
Project will continue focusing our efforts on building a movement with
community-led organizations working the frontlines to dismantle systemic racism
and oppression through the use of technology we collectively own and control.

While state repression, violence and surveillance have been with us for
centuries, as a product of settler colonialism, slavery, imperialism, and
capitalism, the current role of technology in this repression is novel and
insidiously integrated with our daily lives. Our fight brings politics to the
technology and technology to the politics - the struggles are inseparable.
Onward!

#BlackLivesMatter #JusticeforGeorgeFloyd #Resist
