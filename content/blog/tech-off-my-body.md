+++
date = "2022-06-27T16:00:08Z"
title = "Get the Tech Off My Body! Recording and resources"
banner = "img/blogs/abortionfunds.png"
+++

Overturning Roe v. Wade in the United States and restricting access to abortion
throughout the Americas is part of a strategy of repression that is focused on
our bodies. The strategy not only targets reproductive justice, but also
transgender rights, the mobility of poor people and people of color, and access
to health care and other critical services. 

On June 21st, 2022, together with [May First](https://mayfirst.coop/) and the
[National Network of Abortion Funds](https://abortionfunds.org), the
Progressive Technology Project organized a webinar to discuss the relationship
between the struggles for reproductive justice, privacy, autonomy, and freedom
from surveillance, asking ourselves: **How can our campaign to re-envision
technology based on consent and liberation contribute to our movement's
struggle for reproductive justice?**

Please [visit the podcast to hear a recording](https://mayfirst.coop/en/audio/tech.off.my.body/) or view the collected resources below.

## Collected Resource from the webinar

 * [Riana's blog post about abortion privacy & encryption](http://cyberlaw.stanford.edu/blog/2022/05/end-roe-will-bring-about-sea-change-encryption-debate)
 * [EFF abortion privacy & security tips](https://www.eff.org/deeplinks/2022/05/digital-security-and-privacy-tips-those-involved-abortion-access)
 * [Talk about abortion criminalization, “Jailed by a Google Search”](https://www.youtube.com/watch?v=RcPpbWurw4U)
 * [Politico article about abortion privacy & security (6/20)](https://www.politico.com/news/2022/06/17/abortion-rights-advocates-digital-security-threats-00040654)
 * [“My Body, My Data Act”](https://sarajacobs.house.gov/news/documentsingle.aspx?DocumentID=542)
 * [Ale Link Tree](https://linktr.ee/emotionalgangzter)
 * [American Dragnet Data-Driven Deportation in the 21st Century](https://www.americandragnet.org/)
 * [Defunding the Dragnet](https://justfutureslaw.org/wp-content/uploads/2021/11/JFL_Report-DefendingTheDragnet_111521.pdf)
 * [End Abusive Surveillance of Immigrant, Black & Brown Communities](https://justfutureslaw.org/wp-content/uploads/2020/12/JFL-Mijente-Immig-Tech-Surveillance-Rec-1.pdf)
 * [Hacking//Hustling](https://hackinghustling.org/)
 * [EFF - Street Level Surveillance](https://www.eff.org/issues/street-level-surveillance)
 * [Detention Watch Network: Defund Hate](https://www.detentionwatchnetwork.org/defundhate)
 * [Mijente No Tech For ICE Campaign](https://notechforice.com)
 * [Time Magazine - Abortion Surveillance](https://time.com/6184111/abortion-surveillance-tech-tracking/)
 * [Digital Security for Abortio Poster](https://digitaldefensefund.org/ddf-artwork-zines/digital-security-for-abortion-and-pregnancy-privacy-poster)
 * [From Data Criminalization to Prison Abolition](https://abolishdatacrim.org/en)
 * [De-criminalize Abortion](https://www.interruptingcriminalization.com/decriminalize-abortion)
 * [Facebook tracking abortion](https://www.engadget.com/facebook-meta-pixel-tracking-websites-abortion-services-185249977.html)
 * [Shout your abortion](https://shoutyourabortion.com/ )
 * [Join Mayfirst](https://mayfirst.coop) or if a member, [get involved](https://mayfirst.coop/en/get-involved)


