+++
title = "The Technology Behind ICE"
date = "2019-01-28T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/tech-behind-ice.png"
+++

What do databases, the cloud and a morally unjust immigration system have in
common? It turns out: quite a lot.

According to a [recent report from mijente](https://mijente.net/notechforICE/),
"technology companies [are] playing an increasingly central role in facilitating
the expansion and acceleration of arrests, detentions, and deportations."

In the course of researching Palantir, the relatively unknown software company
with huge Department of Homeland Security contracts (founded by the famously
right-wing Peter Thiel), mijente discovered that many roads lead back to a much
more familiar company: Amazon.

According to the report:

> Amazon has moved from being the one-stop shop for consumers of every kind to
> the biggest broker of cloud storage space on the planet... AWS serves as the
> key contractor in DHS’ migration of the agency’s $6.8 billion information
> technology (IT) portfolio to the cloud.  Amazon, now the wealthiest company
> on the planet, has more federal authorizations to maintain government data
> from a variety of government agencies than any other tech company ... serving
> as DHS’s database for immigration case management systems and biometric data
> for 230 million unique identities — mostly fingerprint records, alongside
> 36.5 million face records and 2.8 million irises.

Mijente's discovery is particularly troubling given the dependence of the
movement on Amazon. Many organization, such as those using
[NationBuilder](https://nationbuilder.com/subprocessors) or
[SalesForce](https://www.salesforce.com/campaign/aws/), may not even realize
that all of their data is stored on servers run by a company with deep ties to
the Department of Homeland Security.

We suspect that this discovery of a link between a growing corporate technology
behemoth and critical movement issues will be followed by many more similar
discoveries. 

As a movement, we have been focused on our growing dependence on technology.
This anxiety, particularly of older activists around our ability to keep up
with the technology, has obscured a far more troubling trend: the relationship
between technology, capitalism and the state. 

Our movement has proven time and time again that we can not only keep up with
new technology, but can even lead that development. The question that remains
is: what technology are we keeping up with? And how are leading it?

If you are interested in getting involved, the Progressive Technology Project
has been working closely with May First/People Link and the Center for Media
Justice to try and answer this very question via the [Technology and Revolution
Campaign](https://www.techandrev.org/). Drop us a line if you want to join us.
