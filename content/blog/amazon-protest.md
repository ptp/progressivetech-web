+++
title = "Amazon and Palantir feel the heat"
date = "2019-07-12T12:47:08+02:00"
tags = [""]
categories = ["general"]
banner = "img/blogs/alexa.jpg"
+++

Progressive Technology Project staff joined many Powerbase using organizations
this week to protest both Palantir and Amazon's contracts and complicity with
ICE. 

Over a hundred people showed up monday morning in front of Palantir's office.
Several days later, hundreds more protested Amazon's annual developers
conference at the Javitz Center in New York.

Over the last several years, corporate technology's true colors have been
emerging: Facebook's pathological disregard for privacy, Google's employment of
over 120,000 temp and contract workers (more than half their total workforce),
and Yahoo's security breach exposing 3 billion accounts just to name a few. 

However, Amazon's collaboration with ICE to separate families and terrorize
immigrants has become the most egregious of them all - and the movement is
responding.

As we referenced in an [earlier blog](/blog/2019/01/28/tech-behind-ice/), Mijente
launched a campaign last year called [No Tech for
ICE](https://mijente.net/notechforice/) which has been gaining steam ever
since.

We are proud to support this campaign and encourage all PTP allies to do the
same. This kind of complicity with government repression is something we should
all oppose and none of us should be surprised by. It's what technology
corporations do. That's why movement technology organizations like ours exist
and why we are building our own technology infrastructure that is owned and
operated by the movement.
