+++
title = "Spectre and Meltdown: Capitalist values will haunt us for years to come"
date = "2018-01-05T09:07:31+05:00"
tags = ["security"]
categories = ["powerbase"]
banner = "img/blogs/spectre.min.png"
+++

![Casper like ghost - spectre logo](/img/blogs/spectre.min.png)

Two significant technical vulnerabilities have been discovered and were publicly announced on January 3.

They are called Meltdown and Spectre.

Unlike most technical vulnerabilities, which are caused by poorly written software, Meltdown and Spectre are caused by the way computer chips are designed. Therefore, they are much harder to fix.

Using these vulnerabilities, an attacker with access to a computer or server (including remote access) can read information used by other programs on the computer or server.

As you can imagine, this is a first class disaster for computers around the world.

Sadly, this problem should not come as a surprise to any of us.

For the past 25 years, Internet technology has been driven primarily by one force: profit. And, in the quest for faster, cheaper and more powerful technology, good and secure design has been a liability at best. Until we re-organize society around movement values of collaboration and sustainability we are likely to see more disasters along these lines.

How does this affect PowerBase users? Fortunately, the problem is mitigated by the fact that you must already have access to a computer in order to exploit the vulnerability.

At PTP, we carefully lock down access to our servers from the bare metal on up - only PTP staff have shell access. Furthermore, we apply security updates regularly, meaning the chance of compromise is relatively low.

Nonetheless, we will continue to monitor both Spectre and Meltdown and apply all security patches in a way that best protects everyone using our servers.

If you have any questions, please don't hesitate to reply - we are happy to answer any query however small.

If you would like more information - below are two excellent articles.

The Guardian has a [simple, non-technical explanation](https://www.theguardian.com/technology/2018/jan/04/meltdown-spectre-computer-processor-intel-security-flaws-explainer).

For a more technical explanation, see the [Arstechnica article](https://arstechnica.com/gadgets/2018/01/whats-behind-the-intel-design-flaw-forcing-numerous-patches/).

![Staff pulling out hair](/img/blogs/staff-pulling-hair.png)

*Bad security haunts PTP staff!!*
