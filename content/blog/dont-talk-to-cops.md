+++
date = 2023-03-21
title = "Don’t [let your devices] talk to cops"
tags = [ ""]
categories = ["general"]
banner = "img/blogs/stopcopcity.jpg"
+++

**A Joint Statement from Progressive Technology Project and May First**

As part of the mass mobilization to protest the creation of Cop City, a huge
police training facility in Atlanta, 23 more protesters have been charged with
domestic terrorism. This latest round brings the total number of Cop City
protesters facing this dangerous charge to 42.

Make no mistake: by charging political protesters with a crime that carries a
sentence of up to 35 years in prison, the police are resorting to desperate
measures to stop dissent. This tactic has been used in the past and will be
used in the future.

*What’s new, however, is the way our opposition is increasingly using technology
to target us.*

We have all been trained never to talk to the cops - that’s a job for our
movement lawyers. But prosecutors can easily circumvent both us and our lawyers
by accessing our digital records. Most of these records are collected by
commercial technology services specifically designed to surveil our locations
and track our social networks. In the hands of the police, they will be taken
wildly out of context and used to harm us.

**What do we do now?**

The first step: keep organizing! While everyone should make individual
decisions about risks and personal safety, we cannot let the cops intimidate
us.

Second, keep using technology for organizing! We need to know the dangers of
corporate technology and seek alternatives (more about that below), but not let
that derail out work. We can use a harm reduction model for reducing our risks
while balancing our need to communicate with our comrades and the broader
movement.

Third, avoid discussing tactics digitally.

Fourth, use privacy respecting and movement-aligned software and services
whenever possible.

**What is our plan?**

Our movement is largely dependent on corporate services for our communications,
databases and digital storage. These same corporations are designing and
selling to us the very digital tools used by the cops to surveil us, providing
the opposition access to our data by request or by subpoena. But there are
alternatives...

Progressive Technology Project, May First and a broad, international movement
of organizations and activists are building alternative, non-corporate
technology options. We are providing movement-aligned technologies with our
security and organizing needs in mind. Technology is the common thread woven
through everything that we do, and if we want a viable long term strategy for
winning, we need to grow this part of the movement—and quickly.

This is where you come in. Together we can use and improve the technologies
that we rely on for our movement work with an eye towards collective access,
safety, and security. Please join us on this journey; and in the interim, keep
yourselves and your folx safe in the struggle for liberation, justice, and
life.

*Image credit: https://stopcop.city/*
