+++
title = "Venture Capital and Organizing Databases"
date = "2024-06-03T11:27:08-04:00"
tags = ["bigtech"]
categories = ["general"]
banner = "img/blogs/van.jpg"
+++

![Broken Down Van](/img/blogs/van.jpg "")

A couple of articles have come to our attention recently about the politics,
power and money behind the technologies where our movement houses our data.

The first is from Klein and Roth Consulting's recent newsletter entitled [*Do
You Know Who Owns Your CRM (and Why it
Matters)?*](https://us1.campaign-archive.com/?u=23c640fc15bca8deabcbdc872&id=bd778d92fe)
by Haley Bash of Donor Organizer Hub and the second is entitled [*Living with
VANxiety: The Present and Future of Progressive Movement
Tech*](https://micahsifry.com/project/living-with-vanxiety-the-present-and-future-of-progressive-movement-tech/)
by journalist Micah L. Sifry.

## How much does it cost now? How much will it cost later?

We've been tracking the same disturbing trends mentioned in these articles
about private equity firms and corporate conglomerates buying up both large and
well known database systems like ngpVAN, Raiser's Edge, Salsa Labs, and Every
Action as well as dozens of small, independent databases. The story is as ~~old
as time~~ **new as capitalism:** consolidate, lower prices, layoff staff, reduce services,
raise prices. In the tech world, this process is so well known that the newly
invented term to describe it,
"[enshittification](https://en.wikipedia.org/wiki/Enshittification)", was
selected by the American Dialect Society as the word of the year for 2023 .

## "Free" as in free labor

Some large nonprofit networks, who are in partnerships with these firms and
corporations, are going one step further: offering their network members "free"
access to these database platforms. But, as anyone who has participated in the
process of moving to a new database knows: it's a lot of work! In fact, the
first year subscription cost of the move is peanuts compared to the staff labor
involved in transferring data, developing workflows, learning how to use the
database, not to mention the subsequent year subscription costs. The "first
year free" deal is like getting married in exchange for a free honeymoon (no
judgement: fun to watch on TV, but not a great idea in practice).

## Is it a pair of shoes or a relationship?

We all have tight budgets and need to make careful financial decisions.
However, unless you expect your organization to remain static, your database
will be less of a "thing" and more of a relationship with the folks who are
supporting you to ensure your database keeps apace with your organization and
your constantly changing data needs. When considering a new database, it is
worth asking: How easily can I get someone on a call? Do the people answering
support questions understand my organizing model? If I ask a question will I be
answered or mansplained?

## A new movement

The Progressive Technology Project is not alone - we are part of a global
movement of independent, "small" and often nonprofit-run (like in the case of
PTP) tech providers with activist backgrounds that offer a wide range of
services. In North America, for example, there are worker cooperatives like
Electric Embers and Koumbit, there are anarchist collectives like Riseup and
there are also other non-profit membership cooperative organizations like May
First Movement Technology. Together with everyone using our services, we are
building an ecological and sustainble alternative to corporate technology.

*Photo is a modified version of an original by Matthew Trump. Creative Commons Attribution - share alike 3.0 - Unported*
