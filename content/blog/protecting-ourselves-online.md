+++
title = "Keeping Safe: Movement Organizations Face Up to Attacks and Surveillance "
date = "2016-10-05T13:39:46+02:00"
tags = ["security"]
categories = ["general"]
banner = "img/blogs/fbi.png"
+++

> Dealing with harassment and surveillance need to be part of every organizer’s toolkit

This July, PTP asked nonprofit communications expert [Gordon Mayer](https://www.progressivetech.org/about/ptp-team/#gordon) to look at emerging issues we and our partner organizations face. This first piece offers a primer on surveillance and harassment._

Nonprofits have a lot of other things to worry about besides surveillance and online harassment, but with an increase in these kinds of unwanted attention online, what once seemed like paranoia is looking more prudent.

[Progressive Technology Project](https://www.progressivetech.org) has always been security minded. Here’s some information on how PTP keeps your data safe, a couple of stories about abuse and surveillance, and tips on how to be ready for both.

## Safety and Surveillance

![FBI returning server](/img/blogs/fbi.png)

*A motion-triggered camera installed by May First caught officials returning their server several weeks after it mysteriously disappeared. [Watch the full video here](https://mayfirst.org/en/2012/fbi-returns-server/).*

We host [PowerBase](https://ourpowerbase.net) clients' and other data on the servers of [May First / People Link,](https://mayfirst.org/en/index.html) a member-led and movement-building technology provider that protects members from both government surveillance and abuse.

In July, the [Deflect project](https://deflect.ca) based in Canada helped May First/People Link [keep the Black Lives Matter website online during a Distributed Denial of Service](https://www.apc.org/en/pubs/statement-recent-attacks-black-lives-matters-websi) attack, for example. 

They also invest in using their own servers, rather than relying on "virtual" servers from corporate vendors like Amazon, and encrypt the hard disks so data is impenetrable without a password.

Abuse and surveillance are nothing new, though. Consider what happened to May First co-founder and PTP Technology Director [Jamie McClelland](https://www.progressivetech.org/about/ptp-team/#jamie) in 2012: two men in suits showed up at his office in the Sunset Park neighborhood of Brooklyn while he was participating in a staff meeting by conference call. He thought they were proselytizing for religion until one held up his FBI badge.

The agents were tracing the email path of someone who had sent anonymous-email bomb threats to a Pittsburgh university via a server May First/People Link shared with another organization. There was little the group could tell the FBI about the messages.

Weeks went by, then the group’s server suddenly went offline. When they went to investigate at the data warehouse where it lived, it was gone. McClelland and May First were quite surprised when the first thing a motion-sensitive camera aimed at the spot where it had been picked up [footage of FBI agents re-installing their server](https://archive.org/details/FbiReturnsRiseupServerToMayFirstpeopleLinkCabinet).

Corporate demands to remove members’ web content–such as parody or spoof websites–for copyright infringement are the most common harassment they see, McClelland said. The procedures outlined by the [Digital Millennium Copyright Act](https://www.eff.org/issues/dmca) (DMCA), require providers to take down content first and ask questions second. But these cease and desist letters of a spoof site rarely hold up in court, because parody has long been ruled a fair use under the DMCA. A longer list of [government surveillance of May First and its members is also online](https://support.mayfirst.org/wiki/legal).

Security and advancing efforts to develop more movement-owned technology ought to be as much a part of an organization’s IT decisions as cost and ease of use, in McClelland’s view: "I like to watch commercial TV and use social media as entertainment, but we simply can’t depend on these corporate tools to do political organizing," he said on a recent May First webinar about government surveillance ([Listen to the webinar here](https://mayfirst.org/en/audio/)).

Here are some suggestions from McClelland for organizations concerned about surveillance:

1.  **Keep website code up to date.** For those who use WordPress or Drupal to run their websites, keeping the software updated is essential to avoid spammers compromising your website, currently the most common form of abuse May First / People Link sees.
1.  **P****asswords and caution**_**.**_ Remind staff and volunteers to use strong passwords and be on the lookout for phishing (spam that entices you to click a link) and spearfishing (sneakier, personalized bogus emails that also seek to get you to click a link).
1.  **Separate servers for website and database.** "If one goes down, you want the other somewhere else," McClelland says.
1.  **If you have reason to fear your website will be attacked consider using [Deflect](https://deflect.ca/)**. When attackers send huge numbers of queries to a server in an attempt to overwhelm it, the service routes their many requests to other IP addresses, "hiding" the site targeted.

While government surveillance is one threat to be aware of, harassment is another rising concern for organizations politically active online.

## Be Ready for Abuse & Surveillance

Online abuse appears to be on the rise. Efforts over the past two years by social media companies to get a handle on harassment failed to make a big dent, [according to a survey by Rad Campaign, Lincoln Park Strategies, and Craig Newmark of craigconnects](http://onlineharassmentdata.org/2016/) in 2016\.

They found 22% of American Internet users 18 and over had "been bullied, harassed or threatened online, or know someone who has" (Read the [press release here](http://onlineharassmentdata.org/2016/harassment-release.html)).

In 2014, the first time they did this survey, fully a quarter of Americans had been harassed themselves or knew someone who was. The number decreased only slightly despite companies’ investment in addressing the problem, the study’s authors stated. Meanwhile, over the past 2 years political harassment became more prevalent, going from 16% to 30% of those who were harassed themselves or who knew someone who was harassed.

![caaavgurley1](https://www.progressivetech.org/wp-content/uploads/2016/09/caaavgurley1-300x143.jpg)

*CAAAV-Organizing Asian Communities, experienced abuse and attempts to take down their website after calling for Officer Peter Liang to be convicted and imprisoned for the murder of Akai Gurley.*

PowerBase user and PTP partner [CAAAV, Organizing Asian Communities](http://caaav.org/), and its Executive Director Cathy Dang have been on the receiving end of both surveillance and especially toxic abuse over the past two years.

In fall 2014, they came out in support of a conviction for New York City Police [Officer Peter Liang in the killing of Akai Gurley in the stairwell of](http://www.motherjones.com/politics/2016/03/peter-liang-police-conviction-nypd) a New York City apartment building in public-housing.

Some saw CAAAV’s support for Gurley and a stiff sentence for Liang as breaking with their own Asian-American community. Dang’s personal information was exposed online and she and other CAAAV leaders as well as the organization itself became targets of abuse and harassment.

"Everyone needs to be prepared for opposition," Dang said. "I didn’t take security stuff seriously until it happened, now we take it so seriously." Abuse started slowly and came in waves, she recalled, in parallel with developments in Liang’s trial (the officer killed Gurley in November 2014, was convicted of manslaughter, and then found guilty of a lesser charge in February 2016\. He served no prison time but was fired by the police department. [New York City settled with the Gurley family by paying them $4 million in August](https://www.theguardian.com/us-news/2016/aug/16/akai-gurley-new-york-police-shooting-settlement)).

Many accused the group of selling out their own community. Dang personally was the victim of doxing (sharing personal information about her) online. Others tried to hack into and take down the group’s website.

PTP helped provide additional support during this time by monitoring who was logging in to the CAAAV PowerBase site. The group drew on other allies as well to come up with a few key steps to protect their website and their data.

For herself, Dang began using a [Google Voice](https://www.google.com/googlevoice/about.html) number on news releases and other public documents, in order to keep her cell phone private. She downloaded [Signal Private Messenger](https://whispersystems.org/) to send and receive encrypted text messages with co-workers and friends.

CAAAV also responded with 6 steps any organization can take to tighten security:

1.  **All staff switched to two-step verification for their email**. Dang noted she herself has received notifications of someone trying to hack into her email account several times.
2.  **Everyone on staff changed to stronger passwords for email and PowerBase accounts**. Each contains capital and lower-case letters, numbers and characters.
3.  **The group became more vigilant about people accessing google docs.** For example, Dang downloaded and deleted files with names of peoples who had signed on to support the organization.
4.  **They no longer share passwords between staff for their online account**s ("We were just sharing passwords left and right," Dang says).
5.  **Staff keep all the passwords and log interns into systems** for which a password is required.
6.  **Another precaution they took was logging out and shutting down all the office computers at the end of the day**.

Also worth noting, Dang says police officers during this time visited their office with offers to escort them to actions they had just planned, and it soon became clear that the NYPD was monitoring their social media feeds to track CAAAV’s plans for actions.

Has your organization experienced surveillance or harassment? Let us know — we’d like to keep better track of this ([drop us a line to let us know](mailto:communications@progressivetech.org) — we will update this information with your stories, or happy to keep it confidential if you prefer).  

