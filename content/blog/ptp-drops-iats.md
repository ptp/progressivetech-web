+++
title = "Progressive Technology Project Drops iATS Payment Processor for Failing to Support Abortion Providers "
date = "2017-10-05T13:07:31+02:00"
tags = ["iats"]
categories = ["powerbase"]
banner = "img/blogs/payment-processing.jpg"
+++

![Finger with pen hovering over credit card machine](/img/blogs/payment-processing.jpg "By Alex Proimos from Sydney, Australia")

*By Alex Proimos from Sydney, Australia (Payment Processing) [CC BY 2.0](http://creativecommons.org/licenses/by/2.0), via Wikimedia Commons*

*Media Contacts:*

* PTP Executive Director Alice Aguilar: info@progressivetech.org or 612-351-3479
* California Latinas for Reproductive Justice: info@clrj.org or 213-270-5258

*October 5, 2017*

**For Immediate Release**

The Progressive Technology Project today announced that it is no longer using the iATS payment processing service after discovering that the processor’s parent company rejects applicants who provide abortion counseling or services. PTP is encouraging other social justice movement organizations to stand in solidarity and do the same.

iATS is one of several companies that provide credit card payment services to organizations that collect donations or event fees online. Its parent company and primary acquiring bank, First American Payment Systems of Ft. Worth, Texas, has an official policy that it will not process payments for organizations offering reproductive rights counseling or abortion services.


"This policy came as a complete shock to us," said Alice Aguilar, the Progressive Technology Project’s Executive Director. "We discovered the policy inadvertently while helping California Latinas for Reproductive Justice (CLRJ) apply for the iATS service so they could process donations on their web site."


After experiencing an unexpected delay in the processing of CLRJ’s application, PTP pressed iATS for a reason and in response received an official policy from First American Payment Systems. According to the policy (READ THE POLICY HERE), First American has a list of prohibited merchants that includes "Medical Clinic / Clinic – Which provide counseling or perform, in part or whole, any services related to the early termination of pregnancy."


"The ability for people to make decisions about their reproductive lives, including access to abortion, is a human right," said Laura Jiménez, Executive Director with California Latinas for Reproductive Justice. "Discriminatory business practices targeting abortion providers, or abortion rights advocates are part of a larger problem where certain policymakers and business owners use their power to create additional barriers for women to exercise their right to self determination, and we not only refuse to do business with them, but will actively and loudly oppose them."


iATS itself does not have a policy against organizations providing services related to the termination of pregnancy, and uses a different acquiring bank for groups who perform these services.


However, according to Aguilar, "We want nothing to do with First American, or any of its affiliates, including iATS. When your policy explicitly allows gun dealers to sell weapons but rejects groups for providing counseling or services around access to abortion services – it’s a clear values statement that continues to perpetuate the stigma around abortions. When social justice groups are working to provide access to medically safe abortion services for people of color and low-income communites and are strongly advocating against the harshest anti-abortion bills we are seeing today, especially in places such as Texas, First American throws up another barrier for organizations to raise the funds to move this critical work."


