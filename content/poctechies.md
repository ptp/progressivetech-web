+++
title = "People of Color Techie Training Project"
+++

![POC Techie group photo](/img/pages/poctechies.jpg)

The People of Color Techie program provides an intense, supportive, mentor-based training program for activists of color to become professional-level, politically progressive and movement involved technologists while engaging in and confronting racism and race-based exclusion within the radical technology movement.

The program has arisen due to the increasing importance of the Internet to our organizing work, which has led to the critical role of the technologists that are directing this work.

Unfortunately, the techies, the Internet's leaders, are mainly white men. This means that, while people of color can do a lot on the Internet, the management of the machines that serve the Internet; the creation and development of the software that runs it; and the power and ability to control and maintain the technology are usually not in the hands of people of color.

For the most part, progressive techies are very conscious of this and consistently lament the situation. But, up to now, not very much has been done about it.

The situation threatens the movement's successful use of technology in its organizing work and deepens the "digital divide" that has long plagued movement Internet use. Without people of color involved in the maintenance, planning and direction of the Internet it is impossible to provide communities of color the fullest access to this powerful tool and the huge International community that uses it.

PTP is working in partnership with [May First/People Link](https://mayfirst.org/) on this program.

