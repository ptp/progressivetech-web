+++
title = "Next GENeration Technology"
+++

![Presenters during a Technology and Revolution convergence](/img/pages/tech-and-rev.jpg)

The Internet has revolutionized the way we communicate and organize - allowing us to reach millions of people in ways we never though possible just thirty years ago.

However, these advances in organizing are more like unintentional by-products of a cycle of technological development driven by capitalism and corporate interests.

The Progressive Technology Project is working closely with [May First/People Link](https://mayfirst.org/) to support the [Technology and Revolution campaign](https://techandrev.org/) to build dialog among the left around technology strategy and ways we can put our collective muscle together to influence the development of technology in truly revolutionary ways, so our interests in a just and sustainable world are the central focus and goal of technology.

We are also actively experimenting with new technologies, such as peer-to-peer software that is not vulnerable to servers being taken off line, and community wireless innitiatives that can function without an individual consumer dependence on corporate Internet.
