var remoteFormConfig = {
 url: "https://ptp.ourpowerbase.net/civicrm/remoteform",
 id: 23,
 entity: "Profile",
};
if (typeof remoteForm !== "function") {
  document.getElementById("remoteForm").innerHTML = "Oh no! I was not able to display the form! Please check your security settings (for example Privacy Badger) and allow remote javascript to be displayed from ptp.ourpowerbase.net."
  document.getElementById("remoteForm").style.color = "red";
}
else {
  remoteForm(remoteFormConfig);
}
