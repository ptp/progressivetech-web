/*
 * Use ical.min.js to parse and display PTP events
 */

var source = "/events.ics";

function formatDate(startDate, endDate) {
 var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ]

  var startMonth = monthNames[startDate.getMonth()];
  var endMonth = monthNames[endDate.getMonth()];

  if (endMonth == startMonth && endDate.getDate() == startDate.getDate()) {
    // Omit the ending month and date
    return startMonth + " " + startDate.getDate() + " " + formatTime(startDate)
      + " - " + formatTime(endDate); 
  }
  else {
    return startMonth + " " + startDate.getDate() + " " + formatTime(startDate)
      + " - " + endMonth + " " + endDate.getDate()
      + " " + formatTime(endDate);
  }
}


// Thanks https://stackoverflow.com/questions/8888491/how-do-you-display-javascript-datetime-in-12-hour-am-pm-format#8888498
function formatTime(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}


// Get the ical data
$.ajax({
   url:source,
   type:'GET',
   success: function(data){
    var jcalData = ICAL.parse(data);
    var vcalendar = new ICAL.Component(jcalData);
    var vevents = vcalendar.getAllSubcomponents('vevent');
    var length = vevents.length;
    var content = '';
    var max = 4;
    for (var i = 0; i < length; i++) {
      var summary = vevents[i].getFirstPropertyValue('summary');
      var url = vevents[i].getFirstPropertyValue('url');
      var dateStart = new Date(vevents[i].getFirstPropertyValue('dtstart'));
      var dateEnd = new Date(vevents[i].getFirstPropertyValue('dtend'));
      var displayDate = formatDate(dateStart, dateEnd);
      console.log(summary + " " + formatDate(dateStart, dateEnd))
      content += '<div class="ptp-event-summary"><a href="' + url + '">' + summary + '</a></div>';
      content += '<div class="ptp-event-date">' + displayDate + '</div>';
      if (max == i) {
        break;
      }
    }

    if (content) {
      var header = '<h4 class="ptp-events-header">Upcoming events</h4>';
      var footer = '<div id="ptp-events-footer">All entries displayed in central time zone.</div>';
      $("#ptp-events").append(header + content + footer);
    }
  }
});
